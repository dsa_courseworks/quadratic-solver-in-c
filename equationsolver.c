#include <stdio.h>
#include <math.h>

int main ()
{
    float a = 0, b = 0, c = 0, det = 0, x1 = 0, x2 = 0;
    printf("Enter a , b and c for a quadratic equation: \n");
    scanf("%f %f %f" ,&a , &b, &c );
    det = (b * b) - (4 * a * c);
    if (det >= 0) {
        //there exits a square root
        //if det == 0, then we have equal x values
        if (det == 0) {
            x1 = (-b + det) / 2 * a;
            printf("we have similar x1 and x2 \n");
            printf("x1 = x2 = %.2f", x1);
        }
        else
        {
            x1 = (-b + sqrtf(det)) / (2 * a);
            x2 = (-b - sqrtf(det)) / (2 * a);
            printf("we have distinct x1 and x2 \n");
            printf("x1 = %.2f and x2 = %.2f", x1, x2);
        }
    }
    else
    {
        printf("We have complex roots");
    }
    return 0;
}
